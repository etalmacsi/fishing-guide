const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateExpInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.brand = !isEmpty(data.brand) ? data.brand : "";
  data.from = !isEmpty(data.from) ? data.from : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Név megadása kötelező!";
  }

  if (Validator.isEmpty(data.brand)) {
    errors.brand = "Márka megadása kötelező!";
  }

  if (Validator.isEmpty(data.from)) {
    errors.from = "Mióta használja mező megadása kötelező!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
