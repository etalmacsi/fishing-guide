const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";

  if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = " A névnek 2-30 karakter hosszúságúnak kell lennie!";
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = "A név mező kötlező!";
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = "Helytelen e-mail cím!";
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Az e-mail cím megadása kötelező";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Jelszó megadása kötlező";
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "A jelszó minimum 6 karakter hosszúságú!";
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "Jelszó megadása kötlező";
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "A jelszavaknak egyezni kell!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
