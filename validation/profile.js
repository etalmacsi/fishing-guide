const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.handle = !isEmpty(data.handle) ? data.handle : "";
  data.favourite = !isEmpty(data.favourite) ? data.favourite : "";
  data.interests = !isEmpty(data.interests) ? data.interests : "";

  if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
    errors.handle = "Az azonosítonak 2-40 karakter hosszúságúnak kell lennie!";
  }

  if (Validator.isEmpty(data.handle)) {
    errors.handle = "Profil azonosító kötelező!";
  }

  if (Validator.isEmpty(data.favourite)) {
    errors.favourite = "A kedvenc módszer megadása kötelező!";
  }

  if (Validator.isEmpty(data.interests)) {
    errors.interests = "A kedvelt horgászmódszerek megadása kötelező!";
  }

  if (!isEmpty(data.website)) {
    if (!Validator.isURL(data.website)) {
      errors.website = "Nem valós URL";
    }
  }

  if (!isEmpty(data.youtube)) {
    if (!Validator.isURL(data.youtube)) {
      errors.youtube = "Nem valós  URL";
    }
  }

  if (!isEmpty(data.twitter)) {
    if (!Validator.isURL(data.twitter)) {
      errors.twitter = "Nem valós URL";
    }
  }

  if (!isEmpty(data.facebook)) {
    if (!Validator.isURL(data.facebook)) {
      errors.facebook = "Nem valós URL";
    }
  }

  if (!isEmpty(data.instagram)) {
    if (!Validator.isURL(data.instagram)) {
      errors.instagram = "Nem valós URL";
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
