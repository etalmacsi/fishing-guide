const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateExpInput(data) {
  let errors = {};

  data.type = !isEmpty(data.type) ? data.type : "";
  data.brand = !isEmpty(data.brand) ? data.brand : "";

  if (Validator.isEmpty(data.type)) {
    errors.title = "Típus megadása kötelező!";
  }

  if (Validator.isEmpty(data.brand)) {
    errors.brand = "Márka megadása kötelező!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
