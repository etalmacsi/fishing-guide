const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateCatchInput(data) {
  let errors = {};

  data.lakeId = !isEmpty(data.lakeId) ? data.lakeId : "";
  data.species = !isEmpty(data.species) ? data.species : "";
  data.weight = !isEmpty(data.weight) ? data.weight : "";
  data.description = !isEmpty(data.description) ? data.description : "";

  if (Validator.isEmpty(data.lakeId)) {
    errors.lakeId = "A horgásztó nevének megadása kötelező!";
  }
  if (Validator.isEmpty(data.species)) {
    errors.species = "A kifogott halfaj megadása kötelező!";
  }
  if (Validator.isEmpty(data.weight)) {
    errors.weight = "A kifogott hal súlyának megadása kötelező!";
  }
  if (Validator.isEmpty(data.description)) {
    errors.description = "A fogás körülményeinek megadása kötelező!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
