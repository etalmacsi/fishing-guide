const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateLakeInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.location = !isEmpty(data.location) ? data.location : "";
  data.size = !isEmpty(data.size) ? data.size : "";
  data.description = !isEmpty(data.description) ? data.description : "";
  data.fishes = !isEmpty(data.fishes) ? data.fishes : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "A horgásztó nevének megadása kötelező!";
  }
  if (Validator.isEmpty(data.location)) {
    errors.location = "A horgásztó helyének megadása kötelező!";
  }
  if (Validator.isEmpty(data.size)) {
    errors.size = "A horgásztó nagyságának megadása kötelező!";
  }
  if (Validator.isEmpty(data.description)) {
    errors.description = "A horgásztó nevének megadása kötelező!";
  }
  if (Validator.isEmpty(data.fishes)) {
    errors.fishes = "A horgásztóban megtalálható halfajok megadása kötelező!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
