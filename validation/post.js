const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validatePostInput(data) {
  let errors = {};

  data.text = !isEmpty(data.text) ? data.text : "";

  if (!Validator.isLength(data.text, { min: 10, max: 300 })) {
    errors.text =
      "A bejegyzésnek minimum 10, maximum 300 karakterből kell állnia!";
  }

  if (Validator.isEmpty(data.text)) {
    errors.text = "A szöveg mező kötelező!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
