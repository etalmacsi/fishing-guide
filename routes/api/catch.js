const express = require("express");
const router = express.Router();
const multer = require("multer");
const mongoose = require("mongoose");
const passport = require("passport");

//model
const Catch = require("../../models/Catch");
//validation
const validateCatchInput = require("../../validation/catches");
const validatePostInput = require("../../validation/post");

//create stoage for image upload
const storage = multer.diskStorage({
  destination: "./uploads/catch/",
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + ".jpg");
  }
});

const upload = multer({ storage: storage }).single("photo");

//@route    GET api/catches/test
//@desc     tests catches route
//@acces    public
router.get("/test", (req, res) => res.json({ msg: "catches works" }));

//@route    POST  api/catches
//@desc     add a catch
//@acces    private

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    upload(req, res, err => {
      const { errors, isValid } = validateCatchInput(req.body);

      if (err || !isValid) {
        return res.status(400).json(errors);
      }
      let newCatch = {};
      if (req.file) {
        newCatch = new Catch({
          lakeId: req.body.lakeId,
          user: req.user.id,
          userName: req.user.name,
          species: req.body.species,
          weight: req.body.weight,
          description: req.body.description,
          photo: req.file.filename,
          weather: req.body.weather
        });
      } else {
        newCatch = new Catch({
          lakeId: req.body.lakeId,
          user: req.user.id,
          userName: req.user.name,
          species: req.body.species,
          weight: req.body.weight,
          description: req.body.description,

          weather: req.body.weather
        });
      }

      newCatch.save().then(lake => res.json(lake));
    });
  }
);

//@route    GET api/catches
//@desc     get all catches
//@acces    public

router.get("/", (req, res) => {
  Catch.find()
    .then(catches => res.json(catches))
    .catch(err =>
      res.status(404).json({ nokCatchesfound: "Nincsenek regisztrált fogások" })
    );
});

//@route    GET api/catches/:id
//@desc     get catch by id
//@acces    public

router.get("/:id", (req, res) => {
  Catch.findById(req.params.id)
    .then(catches => res.json(catches))
    .catch(err =>
      res.status(404).json({ noklakesfound: "Nincsen ilyen Fogás" })
    );
});

//@route    POST api/catches/comment/:id
//@desc     Add a comment to a catch
//@acces    private

router.post(
  "/comment/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);

    //check validation
    if (!isValid) {
      //if errors, send 400
      return res.status(400).json(errors);
    }

    Catch.findById(req.params.id)
      .then(post => {
        const newComment = {
          text: req.body.text,
          name: req.body.name,
          avatar: req.body.avatar,
          user: req.user.id
        };

        // add to comment array

        post.comments.unshift(newComment);

        //Save

        post.save().then(post => res.json(post));
      })
      .catch(err =>
        res.status(404).json({ postnotfound: "Nincsenek bejegyzések!" })
      );
  }
);

//@route    POST api/posts/like/:id
//@desc     Like a post
//@acces    private

router.post(
  "/like/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id }).then(profile => {
      Catch.findById(req.params.id)
        .then(post => {
          if (
            post.likes.filter(like => like.user.toString() === req.user.id)
              .length > 0
          ) {
            return res.status(400).json({ alreadyliked: "Már like-olta!" });
          }

          post.likes.unshift({ user: req.user.id });

          post.save().then(post => res.json(post));
        })
        .catch(err =>
          res.status(404).json({ postnotfound: "Nincsenek bejegyzések!" })
        );
    });
  }
);

//@route    POST api/posts/unlike/:id
//@desc     Unlike a post
//@acces    private

router.post(
  "/unlike/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id }).then(profile => {
      Catch.findById(req.params.id)
        .then(post => {
          if (
            post.likes.filter(like => like.user.toString() === req.user.id)
              .length === 0
          ) {
            return res
              .status(400)
              .json({ alreadyliked: "Még nem like-olta a bejegyzést" });
          }

          //get the remove index
          const removeIndex = post.likes
            .map(item => item.user.toString())
            .indexOf(req.user.id);

          //splice out of array
          post.likes.splice(removeIndex, 1);

          //Save
          post.save().then(post => res.json(post));
        })
        .catch(err =>
          res.status(404).json({ postnotfound: "Nincsenek bejegyzések!" })
        );
    });
  }
);

module.exports = router;
