const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const multer = require("multer");
const axios = require("axios");

//create stoage for image upload
const storage = multer.diskStorage({
  destination: "./uploads/lakes/",
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + ".jpg");
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage: storage, fileFilter: fileFilter }).single(
  "lakeAvatar"
);

//model
const Lake = require("../../models/lakes");
//validation
const validateLakeInput = require("../../validation/lakes");

//@route    GET api/lakes/test
//@desc     tests lakes route
//@acces    public

router.get("/test", (req, res) => res.json({ msg: "lakes works" }));

//@route    GET api/lakes/lake
//@desc     Add a lake route
//@acces    public

router.post("/lake", (req, res) => {
  upload(req, res, err => {
    const { errors, isValid } = validateLakeInput(req.body);
    let newLake = {};

    // if can't be determined or format not accepted
    if (err || !isValid) {
      return res.status(400).json(errors);
    }

    if (req.file) {
      newLake = new Lake({
        name: req.body.name,
        location: req.body.location,
        size: req.body.size,
        description: req.body.description,
        fishes: req.body.fishes,
        avatar: req.file.filename,
        price: req.body.price
      });
    } else {
      newLake = new Lake({
        name: req.body.name,
        location: req.body.location,
        size: req.body.size,
        description: req.body.description,
        fishes: req.body.fishes,

        price: req.body.price
      });
    }

    newLake.save().then(lake => res.json(lake));
  });
});

//@route    GET api/lakes
//@desc     get all lake
//@acces    public

router.get("/", (req, res) => {
  Lake.find()
    .then(lakes => res.json(lakes))
    .catch(err =>
      res.status(404).json({ noklakesfound: "Nincsenek regisztrált tavak" })
    );
});

//@route    GET api/lakes/:id
//@desc     get all lake
//@acces    public

router.get("/:id", (req, res) => {
  Lake.findById(req.params.id)
    .then(lake => res.json(lake))
    .catch(err =>
      res.status(404).json({ noklakesfound: "Nincsen ilyen Horgász-Tó" })
    );
});

//@route    GET api/lakes/lakedata/:location
//@desc     get lakedata
//@acces    public

router.post("/lakedata", (req, res) => {
  const OPEN_WEATHER_MAP_URL =
    "https://api.openweathermap.org/data/2.5/weather?appid=9e755ceebcac2310fd259f0b4a6a6b0c&units=metric";

  const location = encodeURIComponent(req.body.location);

  let requestURL = `${OPEN_WEATHER_MAP_URL}&q=${location}`;

  axios
    .get(requestURL)
    .then(data => {
      res.json(data.data);
    })
    .catch(err => {
      res.json(err.data);
    });
});

module.exports = router;
