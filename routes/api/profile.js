const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

//load validation

const validateProfileInput = require("../../validation/profile");
const validateEqiInput = require("../../validation/equipment");
const validateBaitsInput = require("../../validation/baits");

const Profile = require("../../models/Profile");
const User = require("../../models/User");

//@route    GET api/profile/test
//@desc     tests profile route
//@acces    public
router.get("/test", (req, res) => res.json({ msg: "profile works" }));

//@route    GET api/profile
//@desc     get current user profile
//@acces    private
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
      .populate("user", ["name", "avatar"])
      .then(profile => {
        if (!profile) {
          errors.noprofile = "Nincs profilja az alábbi felhasználónak!";
          return res.status(404).json(errors);
        }
        res.json(profile);
      })
      .catch(err => res.status(404).json(err));
  }
);

//@route    GET api/profile/handle/:handle
//@desc     get profile by handle
//@acces    public

router.get("/handle/:handle", (req, res) => {
  const errors = {};

  Profile.findOne({ handle: req.params.handle })
    .populate("user", ["name", "avatar"])
    .then(profile => {
      if (!profile) {
        errors.noprofile = "Nincs profilja az alábbi felhasználónak!";
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});

//@route    GET api/profile/user/:user_id
//@desc     get profile by id
//@acces    public

router.get("/user/:user_id", (req, res) => {
  const errors = {};
  Profile.findOne({ user: req.params.user_id })
    .populate("user", ["name", "avatar"])
    .then(profile => {
      if (!profile) {
        errors.noprofile = "Nincs profilja az alábbi felhasználónak!";
        res.status(404).json(errors);
      }
      res.json(profile);
    })
    .catch(err =>
      res
        .status(404)
        .json({ profile: "Nincs profilja az alábbi felhasználónak!" })
    );
});

//@route    GET api/profile/all
//@desc     get all profiles
//@acces    public

router.get("/all", (req, res) => {
  const errors = {};
  Profile.find()
    .populate("user", ["name", "avatar"])
    .then(profiles => {
      if (!profiles) {
        errors.noprofile = "Nincsenek felhasználói profilok!";
        return res.status(404).json(errors);
      }
      res.json(profiles);
    })
    .catch(err =>
      res.status(404).json({ profile: "Nincsenek felhasználói profilok!" })
    );
});

//@route    POST api/profile
//@desc     create or edit user profile
//@acces    private
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProfileInput(req.body);

    // Check validation

    if (!isValid) {
      return res.status(400).json(errors);
    }
    // Get fields
    const profileField = {};
    profileField.user = req.user.id;
    if (req.body.handle) profileField.handle = req.body.handle;
    if (req.body.favourite) profileField.favourite = req.body.favourite;
    if (req.body.website) profileField.website = req.body.website;
    if (req.body.location) profileField.location = req.body.location;
    if (req.body.bio) profileField.bio = req.body.bio;
    if (req.body.fishingTime) profileField.fishingTime = req.body.fishingTime;
    // Interests
    if (typeof req.body.interests !== "undefined") {
      profileField.interests = req.body.interests.split(",");
    }
    //Social
    profileField.social = {};
    if (req.body.youtube) profileField.social.youtube = req.body.youtube;
    if (req.body.twitter) profileField.social.twitter = req.body.twitter;
    if (req.body.facebook) profileField.social.facebook = req.body.facebook;
    if (req.body.instagram) profileField.social.instagram = req.body.instagram;

    Profile.findOne({ user: req.user.id }).then(profile => {
      if (profile) {
        // Update
        Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileField },
          { new: true }
        ).then(profile => res.json(profile));
      } else {
        // create

        // check if handle exist
        Profile.findOne({ handle: profileField.handle }).then(profile => {
          if (profile) {
            errors.hadnle = "Az alábbi azonosítót már használják!";
            res.status(404).json(errors);
          }
          // create or save profile
          new Profile(profileField).save().then(profile => res.json(profile));
        });
      }
    });
  }
);

//@route    POST api/profile/equipment
//@desc     add equipment to profile
//@acces    private

router.post(
  "/equipment",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateEqiInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newEquipment = {
        name: req.body.name,
        brand: req.body.brand,
        from: req.body.from,
        to: req.body.to,
        current: req.body.current,
        description: req.body.description
      };

      // add to experience array

      profile.equipment.unshift(newEquipment);

      profile.save().then(profile => res.json(profile));
    });
  }
);

//@route    POST api/profile/baits
//@desc     add baits to profile
//@acces    private

router.post(
  "/baits",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateBaitsInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newBait = {
        type: req.body.type,
        brand: req.body.brand,
        description: req.body.description
      };

      // add to baits array

      profile.baits.unshift(newBait);

      profile.save().then(profile => res.json(profile));
    });
  }
);

//@route    DELETE api/profile/equipment/:id
//@desc     delete an equipment from prifile
//@acces    private

router.delete(
  "/equipment/:eqi_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        //Get remove index
        const removeIndex = profile.equipment
          .map(item => item.id)
          .indexOf(req.params.eqi_id);

        // Splice out of array

        profile.equipment.splice(removeIndex, 1);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

//@route    DELETE api/profile/baits/:baits_id
//@desc     delete an baits from prifile
//@acces    private

router.delete(
  "/baits/:baits_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        //Get remove index
        const removeIndex = profile.baits
          .map(item => item.id)
          .indexOf(req.params.baits_id);

        // Splice out of array

        profile.baits.splice(removeIndex, 1);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

//@route    DELETE api/profile/
//@desc     delete user and profileprifile
//@acces    private

router.delete(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOneAndRemove({ user: req.user.id }).then(() => {
      User.findOneAndRemove({ _id: req.user.id }).then(() =>
        res.json({ succes: true })
      );
    });
  }
);

module.exports = router;
