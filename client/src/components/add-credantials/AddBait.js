import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import TexAreaFieldGroup from "../common/TextAreaFieldGroup";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addBait } from "../../actions/profileActions";

class AddBait extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      brand: "",
      description: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const baitData = {
      type: this.state.type,
      brand: this.state.brand,
      description: this.state.description
    };

    this.props.addBait(baitData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    const options = [
      { label: "Pellet", value: "Pellet" },
      { label: "Bojli", value: "Bojli" },
      { label: "Magvak", value: "Magvak" },
      { label: "Élő csali", value: "Élő csali" },
      { label: "Műcsali", value: "Műcsali" },
      { label: "Gumihal", value: "Műcsali" }
    ];
    return (
      <div className="add-bait">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Vissza
              </Link>
              <h1 className="display-4 text-center">Csalik hozzáadása</h1>
              <p className="lead text-center">
                A horgászatban felhasznált csalik
              </p>
              <small className="d-block pb-3">* = Kötelező mezők</small>
              <form onSubmit={this.onSubmit}>
                <SelectListGroup
                  placeholder="*Típus"
                  name="type"
                  value={this.state.type}
                  options={options}
                  onChange={this.onChange}
                  error={errors.type}
                />

                <TextFieldGroup
                  placeholder="*Márka"
                  name="brand"
                  value={this.state.brand}
                  onChange={this.onChange}
                  error={errors.brand}
                />

                <TexAreaFieldGroup
                  placeholder="Rövid leírás"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="Érdekesség a csaliról"
                />
                <input
                  type="submit"
                  value="Hozzáadás"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddBait.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addBait }
)(withRouter(AddBait));
