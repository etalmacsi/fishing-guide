import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import TexAreaFieldGroup from "../common/TextAreaFieldGroup";
import FileInput from "../common/FileInput";
import { addLake } from "../../actions/lakeActions";

class AddLake extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      location: "",
      size: "",
      description: "",
      fishes: "",
      avatar: "",
      price: "",
      selectedFile: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.fileSelect = this.fileSelect.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const lakeData = {
      name: this.state.name,
      location: this.state.location,
      size: this.state.size,
      description: this.state.description,
      fishes: this.state.fishes,
      filename: this.state.filename,
      file: this.state.selectedFile,
      price: this.state.price
    };
    this.props.addLake(lakeData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  fileSelect(e) {
    this.setState({
      selectedFile: e.target.files[0]
    });
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="add-lake">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/lakes" className="btn btn-light">
                Vissza a Horgásztavakhoz
              </Link>
              <h1 className="display-4 text-center">Hoorgásztó hozzáadása</h1>
              <p className="lead text-center">Hoorgásztó hozzáadása</p>
              <small className="d-block pb-3">* = Kötelező mezők</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="*Horgásztó neve"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  error={errors.name}
                />
                <TextFieldGroup
                  placeholder="*Horgásztó elhelyezkedése"
                  name="location"
                  value={this.state.location}
                  onChange={this.onChange}
                  error={errors.location}
                />
                <TextFieldGroup
                  placeholder="*Horgásztó mérete"
                  name="size"
                  value={this.state.size}
                  onChange={this.onChange}
                  error={errors.size}
                />
                <TexAreaFieldGroup
                  placeholder="*Horgásztó rövid bemutatása"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="Mutassa be röviden a horgásztavat"
                />

                <TextFieldGroup
                  placeholder="*Horgásztóban fogható halfajok"
                  name="fishes"
                  value={this.state.fishes}
                  onChange={this.onChange}
                  error={errors.fishes}
                />

                <TextFieldGroup
                  placeholder="Napijegy ára"
                  name="price"
                  value={this.state.price}
                  onChange={this.onChange}
                  error={errors.price}
                />
                <p>Kép feltöltése a hotgásztóról</p>
                <FileInput
                  name="avatar"
                  value={this.state.avatar}
                  onChange={this.fileSelect}
                  error={errors.avatar}
                />

                <input
                  type="submit"
                  value="Hozzáadás"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddLake.propTypes = {
  addLake: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  lakes: state.lakes,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addLake }
)(AddLake);
