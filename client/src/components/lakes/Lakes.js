import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getLakes } from "../../actions/lakeActions";
import Spinner from "../common/Spinner";
import LakeItem from "./LakeItem";
import { Link } from "react-router-dom";

class Lakes extends Component {
  componentDidMount() {
    this.props.getLakes();
  }

  render() {
    const { lakes, loading } = this.props.lake;

    let lakesContent;

    if (lakes === null || loading) {
      lakesContent = <Spinner />;
    } else {
      if (lakes.length > 0) {
        lakesContent = lakes.map(lake => (
          <LakeItem key={lake._id} lake={lake} />
        ));
      } else {
        lakesContent = <h4>Nincsen regisztrált Horgásztó</h4>;
      }
    }

    return (
      <div className="profiles">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Link to="/add-lake" className="btn btn-light mb-3 float-left">
                Horgásztó regisztrálása
              </Link>
            </div>
            <div className="col-md-12">
              <h1 className="display-4 text-center">Horgász Tavak</h1>
              <p className="lead text-center">
                Bongésszen a horgásztavak között
              </p>

              {lakesContent}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Lakes.propTypes = {
  getLakes: PropTypes.func.isRequired,
  lake: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  lake: state.lake
});

export default connect(
  mapStateToProps,
  { getLakes }
)(Lakes);
