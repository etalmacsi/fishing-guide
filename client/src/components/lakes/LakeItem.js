import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import isEmpty from "../../validation/is-empty";

class LakeItem extends Component {
  render() {
    const { lake } = this.props;

    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-2">
            {lake.avatar ? (
              <img
                src={require(`../../../../uploads/lakes/${lake.avatar}`)}
                alt="lakePicture"
              />
            ) : (
              <img src={require("../../img/fishing.png")} alt="lakePicture" />
            )}
          </div>
          <div className="col-lg-6 col-md-4 col-8">
            <h3>{lake.name}</h3>
            <p>{lake.location}</p>
            <p>{isEmpty(lake.size) ? null : <span>{lake.size}</span>}</p>
            <Link to={`/lake/${lake._id}`} className="btn btn-info">
              Horgásztó megtekintése
            </Link>
          </div>
          <div className="col-md-4 d-none d-md-block">
            <p>{lake.description}</p>
            <p>{lake.fishes}</p>
            <p>{lake.location}</p>
          </div>
        </div>
      </div>
    );
  }
}

LakeItem.propTypes = {
  lake: PropTypes.object.isRequired
};

export default LakeItem;
