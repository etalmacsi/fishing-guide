import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getData, clearData } from "../../actions/lakeActions";
import GoogleMapReact from "google-map-react";

class LakeData extends Component {
  componentDidMount() {
    this.props.getData(this.props.location);
  }
  componentWillUnmount() {
    this.props.clearData();
  }
  render() {
    const { lakeData } = this.props;
    let content = null;
    if (lakeData.lakeData) {
      const center = {
        lat: lakeData.lakeData.coord.lat,
        lng: lakeData.lakeData.coord.lon
      };
      content = (
        <div className="row">
          <div className="col-md-6">
            <div style={{ height: "100%", width: "100%" }}>
              <GoogleMapReact defaultCenter={center} defaultZoom={13} />
            </div>
          </div>

          <div className="col-md-6">
            <div className="card card-body bg-light mb-3">
              <h3 className="text-center text-info">
                {" "}
                Jelenlegi időjárás adatok
              </h3>

              <p className="lead">
                {" "}
                Hőmérséklet:{lakeData.lakeData.main.temp}{" "}
              </p>
              <p className="lead">
                {" "}
                Légynyomás:{lakeData.lakeData.main.pressure}{" "}
              </p>
              <p className="lead">
                Szélerősség: {lakeData.lakeData.wind.speed}
              </p>
              <p className="lead">
                {" "}
                Felhőzet:{lakeData.lakeData.weather[0].description}{" "}
              </p>
            </div>
          </div>
        </div>
      );
    }

    return <div>{content}</div>;
  }
}

LakeData.protoTypes = {
  getData: PropTypes.func.isRequired,
  lake: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  lakeData: state.lake
});

export default connect(
  mapStateToProps,
  { getData, clearData }
)(LakeData);
