import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getLake } from "../../actions/lakeActions";
import { getCatches } from "../../actions/catchesActions";
import { Link } from "react-router-dom";
import Spinner from "../common/Spinner";
import LakeInfo from "./LakeInfo";
import LakeData from "./LakeData";
import CatchItem from "../catches/CatchItem";

class Lake extends Component {
  componentWillMount() {
    if (this.props.match.params.id) {
      this.props.getLake(this.props.match.params.id);
      this.props.getCatches();
    }
  }

  render() {
    const { lake, loading } = this.props.lake;
    const { catches } = this.props;

    let lakeContent = <Spinner />;

    if (lake === null || loading) {
      lakeContent = <Spinner />;
    } else if (lake.name && catches.catches.length > 0) {
      lakeContent = (
        <div>
          <LakeInfo lake={lake} />
          <h2 className="text-center text-info">Adatok</h2>
          <LakeData location={lake.location} />
          <h2 className="text-center text-info">Fogások a horgásztóról</h2>

          {//eslint-disable-next-line
          catches.catches.map(catches => {
            //eslint-disable-next-line
            if (catches.lakeId === lake.name) {
              return <CatchItem key={catches._id} catchItem={catches} />;
            }
          })}
        </div>
      );
    }

    return (
      <div>
        <div className="row">
          <div className="col-md-6">
            <Link to="/lakes" className="btn btn-light mb-3 float-left">
              Vissza a Horgásztavakhoz
            </Link>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12"> {lakeContent}</div>
        </div>
      </div>
    );
  }
}

Lake.protoTypes = {
  getLake: PropTypes.func.isRequired,
  clearData: PropTypes.func.isRequired,
  lake: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  lake: state.lake,
  catches: state.catches
});

export default connect(
  mapStateToProps,
  { getLake, getCatches }
)(Lake);
