import React, { Component } from "react";
import isEmpty from "../../validation/is-empty";

class LakeInfo extends Component {
  render() {
    const { lake } = this.props;

    return (
      <div className="row">
        <div className="col-md-6">
          {lake.avatar ? (
            <img
              src={require(`../../../../uploads/lakes/${lake.avatar}`)}
              alt="lakePicture"
            />
          ) : (
            <img src={require("../../img/fishing.png")} alt="lakePicture" />
          )}
        </div>
        <div className="col-md-6">
          <div className="card card-body bg-light mb-3">
            <h3 className="text-center text-info">{lake.name} adatlapja</h3>

            <p className="lead"> Elhelyezkedés: {lake.location}</p>
            <p className="lead"> Méret: {lake.size}</p>

            {isEmpty(lake.price) ? null : (
              <p className="lead"> Napijegy: {lake.price} Ft</p>
            )}

            <hr />
            <h3 className="text-center text-info">Egyéb Adatok</h3>
            <p className="lead">Leírás: {lake.description}</p>
            <p className="lead"> Fogható halfajok: {lake.fishes}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default LakeInfo;
