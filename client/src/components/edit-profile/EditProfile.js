import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import InputGroup from "../common/InputGroup";
import SelectListGroup from "../common/SelectListGroup";
import { createProfile, getCurrentProfile } from "../../actions/profileActions";
import isEmpty from "../../validation/is-empty";
import { Link } from "react-router-dom";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displaySocialInputs: false,
      handle: "",
      favourite: "",
      website: "",
      location: "",
      fishingTime: "",
      interests: "",
      bio: "",
      twitter: "",
      facebook: "",
      youtube: "",
      instagram: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      //bring interests array back to CSV

      const interestsCSV = profile.interests.join(",");

      //if profile field not exist,make empyty string

      profile.favourite = !isEmpty(profile.favourite) ? profile.favourite : " ";
      profile.website = !isEmpty(profile.website) ? profile.website : " ";
      profile.location = !isEmpty(profile.location) ? profile.location : " ";
      profile.bio = !isEmpty(profile.bio) ? profile.bio : " ";
      profile.social = !isEmpty(profile.social) ? profile.social : {};
      profile.twitter = !isEmpty(profile.social.twitter)
        ? profile.social.twitter
        : " ";
      profile.facebook = !isEmpty(profile.social.facebook)
        ? profile.social.facebook
        : " ";
      profile.youtube = !isEmpty(profile.social.youtube)
        ? profile.social.youtube
        : " ";
      profile.instagram = !isEmpty(profile.social.instagram)
        ? profile.social.instagram
        : " ";

      // set components field state

      this.setState({
        handle: profile.handle,
        favourite: profile.favourite,
        website: profile.website,
        location: profile.location,
        fishingTime: profile.fishingTime,
        interests: interestsCSV,
        bio: profile.bio,
        twitter: profile.twitter,
        facebook: profile.facebook,
        youtube: profile.youtube,
        instagram: profile.instagram
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const profileData = {
      handle: this.state.handle,
      favourite: this.state.favourite,
      website: this.state.website,
      location: this.state.location,
      fishingTime: this.state.fishingTime,
      interests: this.state.interests,
      bio: this.state.bio,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
      youtube: this.state.youtube,
      instagram: this.state.instagram
    };

    this.props.createProfile(profileData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors, displaySocialInputs } = this.state;

    let socialInputs;

    if (displaySocialInputs) {
      socialInputs = (
        <div>
          <InputGroup
            placeholder="Twitter profile URL"
            name="twitter"
            icon="fab fa-twitter"
            value={this.state.twitter}
            onChange={this.onChange}
            error={errors.twitter}
          />
          <InputGroup
            placeholder="Facebook profile URL"
            name="facebook"
            icon="fab fa-facebook"
            value={this.state.facebook}
            onChange={this.onChange}
            error={errors.facebook}
          />
          <InputGroup
            placeholder="Youtube profile URL"
            name="youtube"
            icon="fab fa-youtube"
            value={this.state.youtube}
            onChange={this.onChange}
            error={errors.youtube}
          />
          <InputGroup
            placeholder="Instagram profile URL"
            name="instagram"
            icon="fab fa-instagram"
            value={this.state.instagram}
            onChange={this.onChange}
            error={errors.instagram}
          />
        </div>
      );
    }

    //Select options for status
    const options = [
      { label: "Adja meg mióta horgászik", value: 0 },
      { label: "Kevesebb, mint 1 év", value: "Kevesebb, mint 1 év" },
      { label: "2 éve", value: "2 éve" },
      { label: "5 éve", value: "5 éve" },
      { label: "10 éve", value: "10 éve" },
      { label: "Több, mint 10 éve", value: "Több, mint 10 éve" }
    ];
    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-ato">
              <Link to="/dashboard" className="btn btn-light">
                Vissza
              </Link>
              <h1 className="display-4 text-center">Profil módoítása</h1>
              <small className="d-block pb-3">*= Kötelező mezők</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Profil azonosító"
                  name="handle"
                  value={this.state.handle}
                  onChange={this.onChange}
                  error={errors.handle}
                  info="Egyedi profil azonosító"
                />
                <SelectListGroup
                  placeholder="Válassza ki mióta horgászik"
                  name="fishingTime"
                  value={this.state.fishingTime}
                  onChange={this.onChange}
                  error={errors.fishingTime}
                  options={options}
                  info="Adja meg mióta horgászik"
                />
                <TextFieldGroup
                  placeholder="Kedvenc horgászmódszer"
                  name="favourite"
                  value={this.state.favourite}
                  onChange={this.onChange}
                  error={errors.favourite}
                  info="Adja meg milyen horgászmódszerrel horgászik legszívesebben"
                />
                <TextFieldGroup
                  placeholder="Weboldal"
                  name="website"
                  value={this.state.website}
                  onChange={this.onChange}
                  error={errors.website}
                  info="Adja meg weboldalát, ha van"
                />
                <TextFieldGroup
                  placeholder="Hely"
                  name="location"
                  value={this.state.location}
                  onChange={this.onChange}
                  error={errors.location}
                  info="Adja meg, melyik térségben szokott horgászni"
                />
                <TextFieldGroup
                  placeholder="* Interests"
                  name="interests"
                  value={this.state.interests}
                  onChange={this.onChange}
                  error={errors.interests}
                  info="Kérem adja meg milyen horgászmodszerekkel szokott horgászni(pl: Feeder,Bojlis,...)"
                />

                <TextAreaFieldGroup
                  placeholder="Rövid leírás"
                  name="bio"
                  value={this.state.bio}
                  onChange={this.onChange}
                  error={errors.bio}
                  info="Mutatkozz be"
                />
                <div className="mb-3">
                  <button
                    type="button"
                    onClick={() => {
                      this.setState(prevState => ({
                        displaySocialInputs: !prevState.displaySocialInputs
                      }));
                    }}
                    className="btn btn-light"
                  >
                    Adjon hozzá egyéb eléréseket
                  </button>
                  <span className="text-muted">Nem kötelező</span>
                </div>
                {socialInputs}
                <input
                  type="submit"
                  value="submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
EditProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { createProfile, getCurrentProfile }
)(withRouter(EditProfile));
