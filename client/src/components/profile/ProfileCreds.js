import React, { Component } from "react";
import Moment from "react-moment";

class ProfileCreds extends Component {
  render() {
    const { baits, equipment } = this.props;

    const baitItems = baits.map(bait => (
      <li key={bait._id} className="list-group-item">
        <h4>{bait.type}</h4>
        <p>
          {" "}
          <strong>brand:</strong> {bait.brand}
        </p>
        <p>
          {bait.description === "" ? null : (
            <span>
              <strong>Leírás: </strong>
              {bait.description}
            </span>
          )}
        </p>
      </li>
    ));

    const equItems = equipment.map(equ => (
      <li key={equ._id} className="list-group-item">
        <h4>{equ.name}</h4>
        <p>
          <Moment format="YYYY/MM/DD">{equ.from}</Moment> -
          {equ.to === null ? (
            "Now"
          ) : (
            <Moment format="YYYY/MM/DD">{equ.to}</Moment>
          )}
        </p>
        <p>
          {" "}
          <strong>Márka:</strong> {equ.brand}
        </p>
        <p>
          {equ.description === "" ? null : (
            <span>
              <strong>Leírás: </strong>
              {equ.description}
            </span>
          )}
        </p>
      </li>
    ));

    return (
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-center text-info">Felszerelés</h3>
          {equItems.length > 0 ? (
            <ul className="list-group">{equItems}</ul>
          ) : (
            <p className="text-center">Nincs felszerelése</p>
          )}
        </div>
        <div className="col-md-6">
          <h3 className="text-center text-info">Csalik</h3>
          {baitItems.length > 0 ? (
            <ul className="list-group">{baitItems}</ul>
          ) : (
            <p className="text-center"> Nincs csalija</p>
          )}
        </div>
      </div>
    );
  }
}

export default ProfileCreds;
