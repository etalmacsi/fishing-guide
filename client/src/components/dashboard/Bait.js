import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteBait } from "../../actions/profileActions";

class Education extends Component {
  onDeleteClick(id) {
    this.props.deleteBait(id);
  }
  render() {
    const bait = this.props.bait.map(bait => (
      <tr key={bait.id}>
        <td>{bait.type}</td>
        <td>{bait.brand}</td>
        <td>{bait.description}</td>
        <td>
          <button
            className="btn btn-danger"
            onClick={this.onDeleteClick.bind(this, bait._id)}
          >
            Törlés
          </button>
        </td>
      </tr>
    ));
    return (
      <div>
        <h4 className="mb-4">Csali</h4>
        <table className="table">
          <thead>
            <tr>
              <th>Típus</th>
              <th>Márka</th>
              <th>Leírás</th>
              <th />
            </tr>
            {bait}
          </thead>
        </table>
      </div>
    );
  }
}

Education.propTypes = {
  deleteBait: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteBait }
)(Education);
