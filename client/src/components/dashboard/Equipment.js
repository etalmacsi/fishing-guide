import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
import { deleteEquipment } from "../../actions/profileActions";

class Equipment extends Component {
  onDeleteClick(id) {
    this.props.deleteEquipment(id);
  }
  render() {
    const equipment = this.props.equipment.map(eq => (
      <tr key={eq.id}>
        <td>{eq.name}</td>
        <td>{eq.brand}</td>
        <td>
          <Moment format="YYYY/MM/DD">{eq.from}</Moment>-
          {eq.to === null ? (
            "Now"
          ) : (
            <Moment format="YYYY/MM/DD">{eq.to}</Moment>
          )}
        </td>
        <td>
          <button
            className="btn btn-danger"
            onClick={this.onDeleteClick.bind(this, eq._id)}
          >
            Törlés
          </button>
        </td>
      </tr>
    ));
    return (
      <div>
        <h4 className="mb-4">Felszerelés</h4>
        <table className="table">
          <thead>
            <tr>
              <th>Megnevezés</th>
              <th>Gyártó</th>
              <th>Mióta</th>
              <th />
            </tr>
            {equipment}
          </thead>
        </table>
      </div>
    );
  }
}

Equipment.propTypes = {
  deleteEquipment: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteEquipment }
)(Equipment);
