import React from "react";
import { Link } from "react-router-dom";

const ProfileActions = () => {
  return (
    <div className="btn-group mb-4" role="group">
      <Link to="/edit-profile" className="btn btn-light">
        <i className="fas fa-user-circle text-info mr-1" /> Profil módosítása
      </Link>
      <Link to="/add-equipment" className="btn btn-light">
        Felszerlés hozzáadása
      </Link>
      <Link to="/add-bait" className="btn btn-light">
        Csali hozzáadása
      </Link>
    </div>
  );
};

export default ProfileActions;
