import React, { Component } from "react";
import PropTypes from "prop-types";
import CatchCommentItem from "./CatchCommentItem";

class CatchCommentFeed extends Component {
  render() {
    const { comments, postId } = this.props;
    return comments.map(comment => (
      <CatchCommentItem key={comments._id} comment={comment} postId={postId} />
    ));
  }
}

CatchCommentFeed.propTypes = {
  comments: PropTypes.array.isRequired
};
export default CatchCommentFeed;
