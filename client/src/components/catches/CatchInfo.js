import React, { Component } from "react";

export default class CatchInfo extends Component {
  render() {
    const { catchInfo } = this.props;
    return (
      <div className="row">
        <div className="col-md-6">
          {catchInfo.photo ? (
            <img
              src={require(`../../../../uploads/catch/${catchInfo.photo}`)}
              alt=""
            />
          ) : (
            <img src={require("../../img/fish.png")} alt="" />
          )}
        </div>
        <div className="col-md-6">
          <div className="card card-body bg-light mb-3">
            <h3 className="text-center text-info">
              {" "}
              {catchInfo.userName} fogása
            </h3>

            <p className="lead"> Halfaj: {catchInfo.species} </p>
            <p className="lead"> Súly: {catchInfo.weight} </p>
            <p className="lead"> Tó: {catchInfo.lakeId} </p>

            <hr />
            <h3 className="text-center text-info">Fogás körülményei</h3>
            <p className="lead">Leírás: {catchInfo.description}</p>
            <p className="lead"> Időjárás: {catchInfo.weather}</p>
          </div>
        </div>
      </div>
    );
  }
}
