import React, { Component } from "react";
import Spinner from "../common/Spinner";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getCatch } from "../../actions/catchesActions";
import { Link } from "react-router-dom";
import CatchInfo from "./CatchInfo";
import CatchCommentForm from "./CatchCommentForm";
import CatchCommentFeed from "./CatchCommentFeed";

class Catch extends Component {
  componentWillMount() {
    if (this.props.match.params.id) {
      this.props.getCatch(this.props.match.params.id);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.catches.catch === null && this.props.profile.loading) {
      this.props.history.push("/not-found");
    }
  }
  render() {
    const { catches } = this.props;
    const { loading } = this.props.catches;
    const { isAuthenticated } = this.props.auth;

    let catchContent = <Spinner />;

    if (catches === null || loading) {
      catchContent = <Spinner />;
    } else if (catches.catch.userName) {
      catchContent = (
        <div>
          <CatchInfo catchInfo={catches.catch} />
          {isAuthenticated ? (
            <div>
              {" "}
              <h2 className="display-4 text-center">Hozzászólások</h2>
              <CatchCommentForm catchId={catches.catch._id} />
            </div>
          ) : null}

          {!isAuthenticated ? (
            <h2 className="display-4 text-center">
              Jelentkezz be, hogy hozzá tudj szólni!
            </h2>
          ) : null}
          <CatchCommentFeed
            catchId={catches.catch._id}
            comments={catches.catch.comments}
          />
        </div>
      );
    }

    return (
      <div>
        <div className="row">
          <div className="col-md-6">
            <Link to="/catches" className="btn btn-light mb-3 float-left">
              Vissza a fogásokhoz
            </Link>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12"> {catchContent}</div>
        </div>
      </div>
    );
  }
}

Catch.protoTypes = {
  getCatch: PropTypes.func.isRequired,
  catches: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  catches: state.catches,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { getCatch }
)(Catch);
