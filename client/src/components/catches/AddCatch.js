import React, { Component } from "react";
import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import TexAreaFieldGroup from "../common/TextAreaFieldGroup";
import FileInput from "../common/FileInput";
import { Link } from "react-router-dom";
import { addCatch } from "../../actions/catchesActions";
import { getLakes } from "../../actions/lakeActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "../common/Spinner";

class AddCatch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lakeId: "",
      species: "",
      weight: "",
      description: "",
      selectedFile: "",
      weather: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.fileSelect = this.fileSelect.bind(this);
  }

  componentWillMount() {
    this.props.getLakes();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const catchData = {
      lakeId: this.state.lakeId,
      species: this.state.species,
      weight: this.state.weight,
      photo: this.state.selectedFile,
      weather: this.state.weather,
      description: this.state.description
    };
    this.props.addCatch(catchData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  fileSelect(e) {
    this.setState({
      selectedFile: e.target.files[0]
    });
  }

  render() {
    const { errors } = this.state;
    const { lake, loading } = this.props;

    let lakeSelection;

    if (lake === null || loading) {
      lakeSelection = <Spinner />;
    } else if (lake.lakes.length > 0) {
      let options = [];

      options.push({
        label: "Válassz horgásztavat",
        value: null
      });
      lake.lakes.forEach(lake => {
        const lakeData = {
          label: lake.name,
          value: lake.lakeId
        };
        options.push(lakeData);
      });

      lakeSelection = (
        <SelectListGroup
          placeholder="*Tó"
          name="lakeId"
          value={this.state.lakeId}
          options={options}
          onChange={this.onChange}
          error={errors.lakeId}
        />
      );
    }

    return (
      <div className="add-bait">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Vissza
              </Link>
              <h1 className="display-4 text-center">Fogás hozzáadása</h1>
              <p className="lead text-center">A fogás adatai</p>
              <small className="d-block pb-3">* = Kötelező mezők</small>
              <form onSubmit={this.onSubmit}>
                {lakeSelection}

                <TextFieldGroup
                  placeholder="*Halfaj"
                  name="species"
                  value={this.state.species}
                  onChange={this.onChange}
                  error={errors.species}
                />

                <TextFieldGroup
                  placeholder="*Súlya"
                  name="weight"
                  value={this.state.weight}
                  onChange={this.onChange}
                  error={errors.weight}
                />

                <TexAreaFieldGroup
                  placeholder="*Rövid leírás"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="A fogás körülményei"
                />

                <TextFieldGroup
                  placeholder="Időjárás"
                  name="weather"
                  value={this.state.weather}
                  onChange={this.onChange}
                  error={errors.weather}
                />
                <p>Kép feltöltése a fogásról</p>
                <FileInput
                  name="photo"
                  value={this.state.photo}
                  onChange={this.fileSelect}
                  error={errors.photo}
                />
                <input
                  type="submit"
                  value="Hozzáadás"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddCatch.propTypes = {
  errors: PropTypes.object.isRequired,
  addCatch: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors,
  lake: state.lake,
  loading: state.loading
});

export default connect(
  mapStateToProps,
  { addCatch, getLakes }
)(AddCatch);
