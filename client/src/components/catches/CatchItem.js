import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import classnames from "classnames";
import { addLike, removeLike } from "../../actions/catchesActions";
import { connect } from "react-redux";

class CatchItem extends Component {
  onLikeClick(id) {
    this.props.addLike(id);
  }

  onUnLikeClick(id) {
    this.props.removeLike(id);
  }

  findUserLike(likes) {
    const { auth } = this.props;
    if (likes.filter(like => like.user === auth.user.id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { catchItem, auth } = this.props;
    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-2">
            {catchItem.photo ? (
              <img
                src={require(`../../../../uploads/catch/${catchItem.photo}`)}
                alt="catcgPicture"
              />
            ) : (
              <img src={require("../../img/fish.png")} alt="catcgPicture" />
            )}
          </div>
          <div className="col-lg-6 col-md-4 col-8">
            <h3>{catchItem.userName} fogása</h3>
            <p>{catchItem.lakeId} tavon</p>
            <Link to={`/catches/${catchItem._id}`} className="btn btn-info">
              Fogás megtekintése
            </Link>
          </div>
          <div className="col-md-4 d-none d-md-block">
            <p>{catchItem.weight}</p>
            <p>{catchItem.species}</p>
            <p>{catchItem.location}</p>{" "}
            {auth.isAuthenticated ? (
              <div>
                <button
                  type="button"
                  className="btn btn-light mr-1"
                  onClick={this.onLikeClick.bind(this, catchItem._id)}
                >
                  <i
                    className={classnames("fas fa-thumbs-up", {
                      "text-info": this.findUserLike(catchItem.likes)
                    })}
                  />
                  <span className="badge badge-light">
                    {" "}
                    {catchItem.likes.length}
                  </span>
                </button>
                <button
                  type="button"
                  className="btn btn-light mr-1"
                  onClick={this.onUnLikeClick.bind(this, catchItem._id)}
                >
                  <i className="text-secondary fas fa-thumbs-down" />
                </button>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

CatchItem.propTypes = {
  auth: PropTypes.object.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { addLike, removeLike }
)(CatchItem);
