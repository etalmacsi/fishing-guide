import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getCatches } from "../../actions/catchesActions";
import Spinner from "../common/Spinner";
import CatchItem from "./CatchItem";

class Catches extends Component {
  componentDidMount() {
    this.props.getCatches();
  }
  render() {
    const { catches, loading } = this.props;

    let catchContent;

    if (catches.catches === null || loading) {
      catchContent = <Spinner />;
    } else {
      if (catches.catches.length > 0) {
        catchContent = catches.catches.map(catches => (
          <CatchItem key={catches._id} catchItem={catches} />
        ));
      } else {
        catchContent = <h4>Nincsen regisztrált fogás</h4>;
      }
    }
    return (
      <div className="profiles">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4 text-center">Fogások</h1>
              <p className="lead text-center">Bongésszen a fogások között</p>
              {catchContent}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Catches.propTypes = {
  getCatches: PropTypes.func.isRequired,
  catches: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  catches: state.catches
});

export default connect(
  mapStateToProps,
  { getCatches }
)(Catches);
