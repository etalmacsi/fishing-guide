import {
  POST_LAKE,
  GET_LAKES,
  GET_LAKE,
  LAKES_LOADING,
  GET_ERRORS,
  GET_LAKE_DATA,
  CLEAR_DATA
} from "../actions/types";

const initialState = {
  lakes: [],
  lake: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_LAKES:
      return {
        ...state,
        lakes: action.payload,
        loading: false
      };
    case LAKES_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_LAKE:
      return {
        ...state,
        lake: action.payload,
        loading: false
      };
    case POST_LAKE:
      return {
        ...state,
        lakes: [action.payload, ...state.lakes]
      };
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    case GET_LAKE_DATA:
      return {
        ...state,
        lakeData: action.payload
      };
    case CLEAR_DATA:
      return {
        ...state,
        lakeData: null,
        lake: null,
        errors: null
      };
    default:
      return state;
  }
}
