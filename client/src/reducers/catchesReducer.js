import {
  GET_ERRORS,
  GET_CATCHES,
  CATCHES_LOADING,
  GET_CATCH
} from "../actions/types";

const initialState = {
  catches: [],
  catch: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_CATCHES:
      return {
        ...state,
        catches: action.payload,
        loading: false
      };
    case GET_CATCH:
      return {
        ...state,
        catch: action.payload,
        loading: false
      };
    case CATCHES_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    default:
      return state;
  }
}
