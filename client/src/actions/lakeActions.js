import {
  POST_LAKE,
  GET_LAKES,
  GET_LAKE,
  LAKES_LOADING,
  GET_ERRORS,
  GET_LAKE_DATA,
  CLEAR_DATA
} from "./types";
import axios from "axios";

// Get lakes
export const getLakes = () => dispatch => {
  dispatch(setLakesLoading());
  axios
    .get("/api/lakes")
    .then(res =>
      dispatch({
        type: GET_LAKES,
        payload: res.data
      })
    )
    .catch(err => dispatch({ type: GET_LAKES, payload: null }));
};

// Set loading
export const setLakesLoading = () => {
  return {
    type: LAKES_LOADING
  };
};

// clear data

export const clearData = () => {
  return {
    type: CLEAR_DATA
  };
};

// Get lake by ID

export const getLake = id => dispatch => {
  dispatch(setLakesLoading());
  axios
    .get(`/api/lakes/${id}`)
    .then(res => dispatch({ type: GET_LAKE, payload: res.data }))
    .catch(err => dispatch({ type: GET_LAKE, payload: null }));
};

// Add lake

export const addLake = (lakeData, history) => dispatch => {
  const config = {
    headers: {
      "content-type": "multipart/form-data"
    }
  };
  const formData = new FormData();

  formData.append("name", lakeData.name);
  formData.append("location", lakeData.location);
  formData.append("size", lakeData.size);
  formData.append("description", lakeData.description);
  formData.append("fishes", lakeData.fishes);
  formData.append("price", lakeData.price);
  formData.append("lakeAvatar", lakeData.file);

  axios
    .post("/api/lakes/lake", formData, config)
    .then(
      res => dispatch({ type: POST_LAKE, payload: res.data }),
      history.push("lakes")
    )
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }));
};

// Get live data to lake

export const getData = location => dispatch => {
  const data = {
    location: location
  };
  axios
    .post("/api/lakes/lakedata", data)
    .then(res => dispatch({ type: GET_LAKE_DATA, payload: res.data }))
    .catch(err => console.log(err));
};
