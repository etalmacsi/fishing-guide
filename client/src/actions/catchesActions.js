import {
  GET_CATCHES,
  GET_ERRORS,
  CATCHES_LOADING,
  GET_CATCH,
  CLEAR_ERRORS
} from "./types";
import axios from "axios";

// Add catch

export const addCatch = (catchData, history) => dispatch => {
  const config = {
    headers: {
      "content-type": "multipart/form-data"
    }
  };
  const formData = new FormData();

  formData.append("lakeId", catchData.lakeId);
  formData.append("species", catchData.species);
  formData.append("weight", catchData.weight);
  formData.append("description", catchData.description);
  formData.append("photo", catchData.photo);
  formData.append("weather", catchData.weather);

  axios
    .post("/api/catches", formData, config)
    .then(res => history.push("catches"))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }));
};

//get all catches

export const getCatches = () => dispatch => {
  dispatch(setCatchesLoading());
  axios
    .get("/api/catches")
    .then(res =>
      dispatch({
        type: GET_CATCHES,
        payload: res.data
      })
    )
    .catch(err => dispatch({ type: GET_CATCHES, payload: null }));
};

// Get catch by id

export const getCatch = id => dispatch => {
  dispatch(setCatchesLoading());
  axios
    .get(`/api/catches/${id}`)
    .then(res =>
      dispatch({
        type: GET_CATCH,
        payload: res.data
      })
    )
    .catch(err => dispatch({ type: GET_CATCH, payload: null }));
};

// Set loading
export const setCatchesLoading = () => {
  return {
    type: CATCHES_LOADING
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const addComment = (commentId, commentData) => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/catches/comment/${commentId}`, commentData)
    .then(res =>
      dispatch({
        type: GET_CATCH,
        payload: res.data
      })
    )
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }));
};

//Add like

export const addLike = id => dispatch => {
  axios
    .post(`/api/catches/like/${id}`)
    .then(res => dispatch(getCatches()))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }));
};

//remove like

export const removeLike = id => dispatch => {
  axios
    .post(`/api/catches/unlike/${id}`)
    .then(res => dispatch(getCatches()))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }));
};
