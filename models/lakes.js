const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create schema

const LakeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  size: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  fishes: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  price: {
    type: String
  },
  base64: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Lake = mongoose.model("lake", LakeSchema);
